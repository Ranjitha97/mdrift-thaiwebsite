from django.db import models

from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.core.fields import StreamField


class HomePage(Page):

    template = "index.html"

class AboutPage(Page):

    template = "about.html"

class TechnologiesPage(Page):

    template = "technologies.html"