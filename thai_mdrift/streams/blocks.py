"""Streamfields live in here."""

from typing import Text

from wagtail.core import blocks
from wagtail.core.blocks.field_block import PageChooserBlock
from wagtail.images.blocks import ImageChooserBlock



class  TitleAndTextBlock(blocks.StructBlock):
    """Title and text and nothin else."""

    title = blocks.CharBlock(required=True, help_text='Add your title')
    text = blocks.TextBlock(required=True, help_text='Add additional text')

    class Meta:
        template = "streams/title_and_text_blocks.html"
        icon ="edit"
        label ="Title and Text"

class CardBlock(blocks.StructBlock):
    """Cards with image and text and button(s)"""

   

    cards = blocks.ListBlock(
        blocks.StructBlock(
            [
                ("image", ImageChooserBlock(required=True)),
                ("text", blocks.TextBlock( max_length=200)),
               
                
                ]
        )
    )
    class Meta:
        template = "streams/card_block.html"
        icon ="placeholder"
        label ="Staff Cards"


class  RichtextBlock(blocks.RichTextBlock):
    """Richtext with all the features."""

    class Meta:
        template = "streams/richtext_block.html"
        icon ="doc-full"
        label ="Full Richtext"


class  SimpleRichtextBlock(blocks.RichTextBlock):
    """Richtext without (limited) all the features."""

    def __init__(self, required=True, help_text=None, editor='default', features=None, **kwargs):
        super().__init__(**kwargs)
        self.features = [ 
            "bold",
            "italic",
            "link",
        ]

    class Meta:
        template = "streams/richtext_block.html"
        icon ="edit"
        label ="Simple Richtext"

class CTABlock(blocks.StructBlock):
    """A simple call to action section"""

    title = blocks.CharBlock(required=True, max_length=60)
    Text = blocks.RichTextBlock(Required=True, features=["bold", "italic"])
    button_page = blocks.PageChooserBlock(required=False)
    button_url = blocks.URLBlock(required=False)
    button_text =blocks.CharBlock(required=True, default='Learn More', max_length=40)

    class Meta:
        template ="streams/cta_block.html"
        icon ="placeholder"
        label = "Call to Action"



